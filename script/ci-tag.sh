#!/bin/sh
# Creates a tag on Gitlab.

# Set git config
git config --global user.name "${GITLAB_USER_NAME}"
git config --global user.email "${GITLAB_USER_EMAIL}"

# Create and push tag
TAG=$(cat package.json | python -c "import json,sys;obj=json.load(sys.stdin);print obj['version'];")
echo "Creating $TAG tag..."
git tag $TAG
git push --tags http://${USERNAME}:${PERSONAL_ACCESS_TOKEN}@gitlab.com/bodhiproject/bodhi-server.git
